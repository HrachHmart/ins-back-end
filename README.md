# README #

Task for the insurance backend. Adding sql files and committing .env file for easy testing. Don't do that in real project.
I don't prefer writing sql queries in the NodeJS project. Always creating Stored procedures and calling them from the code.
Writing queries in the code to make task writing process more fast.

### You can find SQL table files in /sql directory of the project ###

### To run this project? ###

* yarn
* yarn dev

### For testing ###

* yarn
* yarn test

### Swagger URI ###

* /api-docs/v1
