/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : insurance

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 14/11/2021 21:10:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for contractCreatedEvents
-- ----------------------------
DROP TABLE IF EXISTS `contractCreatedEvents`;
CREATE TABLE `contractCreatedEvents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractId` varchar(100) DEFAULT NULL,
  `premium` int(11) DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for contractTerminatedEvents
-- ----------------------------
DROP TABLE IF EXISTS `contractTerminatedEvents`;
CREATE TABLE `contractTerminatedEvents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contractId` varchar(100) DEFAULT NULL,
  `terminationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1;

SET FOREIGN_KEY_CHECKS = 1;
