import 'source-map-support/register';

import { config } from 'dotenv';
config();

import http, { Server } from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';

import mysqlDB from './db/mysql/mysql.db';

import router from './routes';

class App {
  public http: Server;
  public app: express.Application;

  private port: number = Number(process.env.PORT) || 3001;

  constructor() {
    this.app = express();
    this.app.use(bodyParser.json());
    this.app.use(morgan('dev'));
    this.app.use(cors());

    this.http = http.createServer(this.app);

    module.exports = this.http; // for testing
  }

  private listen() {
    this.http.listen(this.port, () => {
      console.log(
        `App listening on the port ${this.port} - ENV ` + process.env.NODE_ENV,
      );
    });
  }

  public async start() {
    this.listen();
    await router(this.app);
  }
}

(async function () {
  const app = new App();

  await mysqlDB.connect();

  await app.start();
})();

process.on('uncaughtException', (e) => {
  console.error(e);
  process.exit(1);
});

process.on('unhandledRejection', (e) => {
  console.error(e);
  process.exit(1);
});
