export const userConfig = {
  name: {
    min: 2,
    max: 20,
  },
  password: {
    min: 8,
    max: 20,
  },
  age: {
    min: 16,
  },
};
