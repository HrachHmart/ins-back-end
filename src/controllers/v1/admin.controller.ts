import MainController from './main.controller';

import typeCheck from '../../helpers/typecheck.helpers';

import { ApiError } from '../../errors';

import contractModel from '../../models/v1/contract.model';

class AdminController extends MainController {
  /**
   * @url /admin/contract/list
   *
   * @param page {number}
   * @param limit {number}
   *
   * @response IContract
   * */
  getContractList = async (req, res, next) => {
    try {
      const page = Number(req.query.page) || 1;
      const limit = Number(req.query.limit) || 10;

      this.successResponse(
        res,
        await contractModel.getContracts(limit, (page - 1) * limit),
      );
    } catch (e) {
      next(e);
    }
  };

  /**
   * @url /admin/contract/create
   *
   * @param {string} contractId
   * @param {number} premium
   * @param {string} startDate
   * */
  createContract = async (req, res, next) => {
    try {
      const { contractId, premium } = typeCheck(req.body, {
        contractId: {
          required: true,
          type: 'string',
        },
        premium: {
          required: true,
          type: 'number',
        },
        startDate: {
          type: 'string',
        },
      });

      if (!premium) {
        throw new ApiError('Premium should be more than 0');
      }

      let startDate = req.body.startDate || Date.now();

      try {
        startDate = new Date(startDate);
      } catch (e) {
        throw new ApiError('Wrong startDate provided');
      }

      if (await contractModel.isContractExists(contractId)) {
        throw new ApiError('Contract already exists');
      }

      await contractModel.createContractEvent(contractId, premium, startDate);

      this.successResponse(
        res,
        await contractModel.getContractById(contractId),
      );
    } catch (e) {
      next(e);
    }
  };

  /**
   * @url /admin/contract/terminate
   *
   * @param {string} contractId
   * @param {string} terminationDate
   * */
  terminateContract = async (req, res, next) => {
    try {
      const { contractId } = typeCheck(req.body, {
        contractId: {
          required: true,
          type: 'string',
        },
        terminationDate: {
          type: 'string',
        },
      });

      let terminationDate = req.body.terminationDate || Date.now();

      try {
        terminationDate = new Date(terminationDate);
      } catch (e) {
        throw new ApiError('Wrong terminationDate provided');
      }

      if (!(await contractModel.isContractExists(contractId))) {
        throw new ApiError('Contract does not exist');
      }

      if (await contractModel.isContractTerminated(contractId)) {
        throw new ApiError('Contract already terminated');
      }

      await contractModel.terminateContractEvent(contractId, terminationDate);

      this.successResponse(res);
    } catch (e) {
      next(e);
    }
  };
}

export default new AdminController();
