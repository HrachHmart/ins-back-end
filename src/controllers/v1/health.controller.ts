import MainController from './main.controller';

class HealthController extends MainController {
  check = (req, res, next) => {
    try {
      this.successResponse(res);
    } catch (e) {
      next(e);
    }
  };
}

export default new HealthController();
