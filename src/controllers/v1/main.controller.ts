import StatusEnum from "../../enums/v1/status.enum";

abstract class MainController {
  successResponse(res, data?) {
    res.status(200).json({
      status: StatusEnum.SUCCESS,
      statusCode: 200,
      data,
    });
  }
}

export default MainController;
