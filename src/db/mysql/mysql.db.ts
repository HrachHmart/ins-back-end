import mysql from 'mysql2/promise';

class MysqlDB {
  public client;

  constructor() {
    this.client = null;
  }

  async connect() {
    console.log('Connecting to MysqlDB...');

    this.client = mysql.createPool({
      host: process.env.MYSQL_HOST,
      port: Number(process.env.MYSQL_PORT) || 3306,
      user: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DB,
    });

    await this.client.execute('select 1+1 as result');
    console.log('Connected to MysqlDB!');
  }
}

export default new MysqlDB();
