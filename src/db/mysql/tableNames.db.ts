const tableNames = {
  CONTRACT_CREATED_EVENTS: 'contractCreatedEvents',
  CONTRACT_TERMINATED_EVENTS: 'contractTerminatedEvents',
};

export default tableNames;
