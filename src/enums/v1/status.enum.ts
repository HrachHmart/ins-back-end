enum StatusEnum {
  SUCCESS = 'success',
  ERROR = 'error',
}

export default StatusEnum;
