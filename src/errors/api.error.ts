class ApiError extends Error {
  private statusCode: number;

  constructor(message = "Bad Request") {
    super();
    this.statusCode = 400;
    this.message = message;
  }
}

export default ApiError;
