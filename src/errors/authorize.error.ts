class AuthorizeError extends Error {
  private statusCode: number;
  constructor(message = 'Not Authorized') {
    super();
    this.statusCode = 401;
    this.message = message;
  }
}

export default AuthorizeError;
