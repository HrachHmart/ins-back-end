class ConflictError extends Error {
  private statusCode: number;

  constructor(message = 'Conflict') {
    super();
    this.statusCode = 409;
    this.message = message;
  }
}

export default ConflictError;
