class ForbiddenError extends Error {
  private statusCode: number;

  constructor(message = 'Forbidden') {
    super();
    this.statusCode = 403;
    this.message = message;
  }
}

export default ForbiddenError;
