import ApiError from "./api.error";
import AuthorizeError from "./authorize.error";
import ForbiddenError from "./forbidden.error";
import StatusEnum from "../enums/v1/status.enum";

function handleError(err, req, res) {
  const statusCode = err.statusCode || 500;
  const message =
    statusCode === 500
      ? "Something went wrong. Please try again."
      : err.message;

  if (statusCode === 500) {
    console.error("Request Error: " + req.originalUrl, err);
  }

  res.status(statusCode).json({
    status: StatusEnum.ERROR,
    statusCode,
    message,
  });
}

export { handleError, ApiError, AuthorizeError, ForbiddenError };
