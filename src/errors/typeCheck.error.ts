class TypeCheckError extends Error {
  private statusCode: number;

  constructor(errors: Array<any>) {
    super();
    this.statusCode = 400;
    this.message = errors.join(', ').trim();
  }
}

export default TypeCheckError;
