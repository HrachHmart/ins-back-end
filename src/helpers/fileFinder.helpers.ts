import { lstatSync, readdirSync } from "fs";

class FileFinder {
  async isDirectory(path): Promise<boolean> {
    try {
      return (await lstatSync(path)).isDirectory();
    } catch (e) {
      return false;
    }
  }

  async getFilesFromPath(path): Promise<string[]> {
    let files: string[] = [];
    const filesInPath: string[] = readdirSync(path);

    if (filesInPath && filesInPath.length) {
      for (const fileOrFolder of filesInPath) {
        if (fileOrFolder.includes(".DS_Store")) {
          continue;
        }

        const joinedPath = path + "/" + fileOrFolder;

        if (await this.isDirectory(joinedPath)) {
          const filesInDir: string[] = await this.getFilesFromPath(joinedPath);
          filesInDir.forEach((fileInDir, index) => {
            filesInDir[index] = fileOrFolder + "/" + fileInDir;
          });

          files = files.concat(filesInDir);
        } else {
          files.push(fileOrFolder);
        }
      }
    }

    return files;
  }
}

export default new FileFinder();
