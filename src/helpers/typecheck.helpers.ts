import TypeCheckError from '../errors/typeCheck.error';

function typeCheck(
  body: { [key: string]: any },
  types: { [key: string]: any },
): any {
  if (!body) {
    throw new TypeCheckError(['Wrong body']);
  }

  if (!types) {
    throw new TypeCheckError(['Wrong types']);
  }

  const errors: Array<any> = [];
  for (const type in types) {
    if (body[type] === undefined) {
      if (types[type].required === true) {
        errors.push(`${type} is required`);
      }
    }

    for (const field in body) {
      if (field === type) {
        if (typeof body[field] === types[type].type) {
          continue;
        }
        errors.push(`Wrong field type for ${type}`);
      }
    }
  }

  if (errors.length !== 0) {
    throw new TypeCheckError(errors);
  }

  return body;
}

export default typeCheck;
