export function onlyLetters(text: string): boolean {
  if (!text) {
    return false;
  }

  const regex = /([A-Za-z])/g;
  return !!text.match(regex);
}

export function onlyNumbers(text: string): boolean {
  if (!text) {
    return false;
  }

  const regex = /([0-9])/g;
  return !!text.match(regex);
}

export function isValidPassword(password: string): boolean {
  if (!password) {
    return false;
  }

  // with Special Chars
  // const regex =
  //   /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,20}$/g;

  const regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/g;

  console.log(password.match(regex));

  return !!password.match(regex);
}

export function isValidEmail(email: string): boolean {
  if (!email) {
    return false;
  }

  const regex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return !!email.match(regex);
}

export function isValidUsername(username: string): boolean {
  if (!username) {
    return false;
  }

  const regex = /([A-Za-z0-9\-\_]+)/;
  return !!username.match(regex);
}

export function isValidAge(birthDate: Date, minAge: number): boolean {
  const ageDifMs = Date.now() - new Date(birthDate).getTime();
  const ageDate = new Date(ageDifMs);
  return Math.abs(ageDate.getUTCFullYear() - 1970) >= minAge;
}

export function isHtmlFormat(text): boolean {
  if (!text) {
    return false;
  }

  const regex = /<\/?[a-z][\s\S]*>/i;
  return !!text.match(regex);
}
