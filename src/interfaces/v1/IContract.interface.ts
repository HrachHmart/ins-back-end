export interface IContractInterface {
  id: number;
  contractId: string;
  premium: number;
  startDate: Date;
  terminationDate: Date;
}
