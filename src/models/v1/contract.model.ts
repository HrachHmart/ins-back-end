import mysqlDB from '../../db/mysql/mysql.db';

import tableNames from '../../db/mysql/tableNames.db';

class ContractModel {
  async getContracts(limit: number = 10, skip: number = 0) {
    const [rows] = await mysqlDB.client.query(
      `SELECT cc.*, ct.terminationDate FROM ${tableNames.CONTRACT_CREATED_EVENTS} cc ` +
        `LEFT JOIN ${tableNames.CONTRACT_TERMINATED_EVENTS} ct ON (cc.contractId = ct.contractId) ORDER BY cc.startDate DESC, cc.id DESC LIMIT ? OFFSET ?;`,
      [limit, skip],
    );

    return rows;
  }

  async isContractExists(contractId: string): Promise<boolean> {
    const [rows] = await mysqlDB.client.query(
      `SELECT EXISTS(SELECT 1 FROM ${tableNames.CONTRACT_CREATED_EVENTS} WHERE contractId=? LIMIT 1) as 'exists';`,
      [contractId],
    );

    const result = rows[0];

    if (result) {
      return !!result.exists;
    }

    return false;
  }

  async isContractTerminated(contractId: string): Promise<boolean> {
    const [rows] = await mysqlDB.client.query(
      `SELECT EXISTS(SELECT 1 FROM ${tableNames.CONTRACT_TERMINATED_EVENTS} WHERE contractId=? LIMIT 1) as 'exists';`,
      [contractId],
    );

    const result = rows[0];

    if (result) {
      return !!result.exists;
    }

    return false;
  }

  async getContractById(contractId: string) {
    const [rows] = await mysqlDB.client.query(
      `SELECT * FROM ${tableNames.CONTRACT_CREATED_EVENTS} WHERE contractId=?;`,
      [contractId],
    );

    return rows[0];
  }

  async getTerminatedContracts(limit: number = 10, skip: number = 0) {
    const [rows] = await mysqlDB.client.query(
      `SELECT * FROM  ${tableNames.CONTRACT_TERMINATED_EVENTS} LIMIT ? OFFSET ?;`,
      [limit, skip],
    );

    return rows[0];
  }

  async createContractEvent(
    contractId: string,
    premium: number,
    startDate: Date,
  ): Promise<any> {
    return mysqlDB.client.query(
      `INSERT INTO ${tableNames.CONTRACT_CREATED_EVENTS} (contractId, premium, startDate) VALUES (?, ?, ?);`,
      [contractId, premium, startDate],
    );
  }

  async terminateContractEvent(
    contractId: string,
    terminationDate: Date,
  ): Promise<any> {
    return mysqlDB.client.query(
      `INSERT INTO ${tableNames.CONTRACT_TERMINATED_EVENTS} (contractId, terminationDate) VALUES (?, ?);`,
      [contractId, terminationDate],
    );
  }
}

export default new ContractModel();
