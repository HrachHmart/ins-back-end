import { readdirSync, writeFile } from 'fs';
import swaggerUi from 'swagger-ui-express';

import { handleError } from '../errors';

import SwaggerGenerator from '../swagger/swagger.generator';

const routes = async (app) => {
  try {
    const versions = readdirSync(__dirname);

    for (const version of versions) {
      if (version === 'index.js' || version === 'index.js.map') {
        continue;
      }

      const swaggerGenerator = new SwaggerGenerator(version);
      await swaggerGenerator.cacheEnums();

      await swaggerGenerator.cacheInterfaces();

      const filePath = __dirname + '/' + version;
      const controllerPath = __dirname + '/../../src/controllers/' + version;

      const routesJSON = {};

      const files = readdirSync(filePath);

      for (const fileName of files) {
        if (fileName.includes('.json') || fileName.includes('.map')) {
          continue;
        }

        const routeLayer = fileName.replace('.route.js', '');

        const base = `/api/${version}` + '/' + routeLayer;
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const routeConfig = require(filePath + '/' + fileName);

        routesJSON[routeLayer] = [];

        if (routeConfig.default && routeConfig.default.stack && routeConfig.default.stack.length) {
          for (const layer of routeConfig.default.stack) {
            const method = Object.keys(layer.route.methods)[0];
            if (method) {
              routesJSON[routeLayer].push(method.toUpperCase() + ' ' + layer.route.path);

              await swaggerGenerator.addPath(routeLayer, method, layer, controllerPath);
            }
          }
        }

        app.use(base, routeConfig.default);
      }

      const routeLayers = Object.keys(routesJSON);

      if (routeLayers.length) {
        for (const routeLayer of routeLayers) {
          swaggerGenerator.addTag(routeLayer);
        }

        const swaggerDocument = await swaggerGenerator.save();

        app.use('/api-docs/' + version, swaggerUi.serve, swaggerUi.setup(swaggerDocument));

        writeFile(
          __dirname + '/../../src/routes/' + version + '/generated.json',
          JSON.stringify(routesJSON, null, 2),
          // eslint-disable-next-line @typescript-eslint/no-empty-function
          () => {},
        );
      }
    }

    app.use((err, req, res, next) => {
      // don't remove "next" param from function
      handleError(err, req, res);
      next();
    });
  } catch (e) {
    console.error('routes/index.ts -> routes() -> Error:', e);
  }
};

export default routes;
