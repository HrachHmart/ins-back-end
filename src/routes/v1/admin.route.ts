import express from 'express';
const router = express.Router();

import adminController from '../../controllers/v1/admin.controller';

router.get('/contract/list', adminController.getContractList);
router.post('/contract/create', adminController.createContract);
router.put('/contract/terminate', adminController.terminateContract);

export default router;
