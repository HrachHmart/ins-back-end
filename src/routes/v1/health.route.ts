import express from 'express';
const router = express.Router();

import healthController from '../../controllers/v1/health.controller';

router.get('/check', healthController.check);

export default router;
