import { writeFileSync, readFileSync } from 'fs';
import { parse } from 'comment-parser';

import baseConfig from './base.json';

import fileFinder from '../helpers/fileFinder.helpers';

class SwaggerGenerator {
  version: string;
  config: { [key: string]: any } = { ...baseConfig };
  isProd: boolean = process.env.NODE_ENV === 'production';
  cachedInterfaces: { [key: string]: any } = {};
  cachedEnums: { [key: string]: any } = {};

  constructor(version: string) {
    this.version = version;

    this.addByVersion();
  }

  addByVersion(): void {
    this.config.schemes = ['http'];

    if (this.isProd) {
      this.config.schemes.unshift('https');
    }

    this.config.host = this.isProd ? '' : 'localhost:' + process.env.PORT;
    this.config.basePath = '/api/' + this.version;
  }

  async cacheInterfaces(): Promise<void> {
    const interfacesFolder = __dirname + '/../../src/interfaces/v1/';
    const filesInFolder = await fileFinder.getFilesFromPath(interfacesFolder);

    if (filesInFolder.length) {
      for (const fileName of filesInFolder) {
        const file = readFileSync(interfacesFolder + fileName).toString();

        const name: any = fileName
          .replace('.interface.ts', '')
          .replace('v1/', '');
        let values: any = file.match(/[A-Za-z][A-Za-z]:([^]+);/gi);

        if (values && values[0]) {
          values = values[0]
            .split('\n')
            .map((line) => line.replace(';', '').split(': '));
        }

        this.cachedInterfaces[name] = {};

        if (values && values.length) {
          values.forEach((value) => {
            if (!this.cachedInterfaces[name]) {
              this.cachedInterfaces[name] = {
                [value[0].trim()]: {
                  type: this.normalizeType(value[1]),
                  enum: this.normalizeEnum(value[1]),
                },
              };
            } else {
              this.cachedInterfaces[name][value[0].trim()] = {
                type: this.normalizeType(value[1]),
                enum: this.normalizeEnum(value[1]),
              };
            }
          });
        }
      }
    }
  }

  async cacheEnums(): Promise<void> {
    const enumsFolder = __dirname + '/../../src/enums/v1/';
    const filesInFolder = await fileFinder.getFilesFromPath(enumsFolder);

    if (filesInFolder.length) {
      for (const fileName of filesInFolder) {
        const file = readFileSync(enumsFolder + fileName).toString();

        let name: any = fileName.replace('.enum.ts', 'Enum').replace('v1/', '');
        let values: any = file.match(/{([^}]*)}/gi);

        if (name) {
          name = name.charAt(0).toUpperCase() + name.slice(1);
        }

        if (values && values[0]) {
          values = values[0]
            .split('\n')
            .map((line) =>
              line.replace(',', '').replace(/'/g, '').split(' = '),
            );
          values = values.splice(1, values.length - 2);
        }

        this.cachedEnums[name] = [];

        if (values && values.length) {
          values.forEach((value) => {
            const numVal = Number(value[1]);
            this.cachedEnums[name].push(isNaN(numVal) ? value[1] : numVal);
          });
        }
      }
    }
  }

  normalizeType(type: string): string {
    if (this.cachedEnums[type]) {
      return typeof this.cachedEnums[type][0];
    }

    switch (type) {
      case 'number':
        return 'integer';
      case 'Date':
        return 'string';
      default:
        return type;
    }
  }

  normalizeEnum(type: string): string {
    if (this.cachedEnums[type]) {
      return this.cachedEnums[type];
    }
  }

  async addPath(
    routeLayer: string,
    method: string,
    expressLayer,
    filePath: string,
  ) {
    const headers = [];
    const routeData = {};
    const isGET = method === 'get';
    let interfaceData;
    let isArray = false;

    const currentPath = '/' + routeLayer + expressLayer.route.path;

    try {
      const file = readFileSync(
        filePath + '/' + routeLayer + '.controller.ts',
      ).toString();

      if (file) {
        const parsed = parse(file);
        for (const data of parsed) {
          let filteredUrl = '';

          const params: any = [];

          if (data.tags && data.tags.length) {
            for (const tagData of data.tags) {
              if (tagData.tag === 'url') {
                filteredUrl = tagData.name;
              } else if (tagData.tag === 'param') {
                if (!isGET) {
                  if (params[0] && params[0].properties) {
                    params[0].properties[tagData.name] = {
                      type: tagData.type,
                    };
                  } else {
                    params[0] = {
                      name: 'body',
                      in: 'body',
                      type: 'object',
                      properties: {
                        [tagData.name]: {
                          type: tagData.type,
                        },
                      },
                    };
                  }
                } else {
                  params.push({
                    in: 'query',
                    type: tagData.type,
                    name: tagData.name,
                    schema: {
                      type: 'string',
                    },
                  });
                }
              } else if (tagData.tag === 'response' && currentPath) {
                const interfaceModel = this.cachedInterfaces[tagData.name];

                isArray = tagData.type === 'array';

                if (interfaceModel) {
                  interfaceData = interfaceModel;
                }
              } else if (tagData.tag === 'header' && currentPath) {
                const name = tagData.name.replace('!', '');
                headers.push({
                  name,
                  required: tagData.name.includes('!'),
                  in: 'header',
                  type: tagData.type,
                });
              }
            }

            if (filteredUrl === currentPath) {
              routeData[filteredUrl] = params;
            }
          }
        }
      }
    } catch (e) {
      console.log('SwaggerGenerator -> addPath Error', e);
    }

    const currentPathData = routeData[currentPath] || [];

    this.config.paths[currentPath] = {
      [method]: {
        tags: [routeLayer],
        consumes: ['application/json'],
        produces: ['application/json'],
        parameters: [
          {
            name: 'authorization',
            in: 'header',
            type: 'string',
          },
        ]
          .concat(headers)
          .concat(currentPathData),
        responses: {
          '200': {
            description: 'success' + (isArray ? ' [array]' : ''),
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'success',
                },
                statusCode: {
                  type: 'integer',
                  default: 200,
                },
                data: interfaceData && {
                  type: 'object',
                  properties: interfaceData,
                },
              },
            },
          },
          '400': {
            description: 'Bad Request',
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'Bad Request',
                },
                statusCode: {
                  type: 'integer',
                  default: 400,
                },
              },
            },
          },
          '401': {
            description: 'Not Authorized',
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'Not Authorized',
                },
                statusCode: {
                  type: 'integer',
                  default: 401,
                },
              },
            },
          },
          '403': {
            description: 'Forbidden',
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'Forbidden',
                },
                statusCode: {
                  type: 'integer',
                  default: 403,
                },
              },
            },
          },
          '409': {
            description: 'Conflict',
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'Conflict',
                },
                statusCode: {
                  type: 'integer',
                  default: 409,
                },
              },
            },
          },
          '500': {
            description: 'Internal Server Error',
            content: 'application/json',
            schema: {
              type: 'object',
              properties: {
                status: {
                  type: 'string',
                  default: 'Internal Server Error',
                },
                statusCode: {
                  type: 'integer',
                  default: 500,
                },
              },
            },
          },
        },
      },
    };
  }

  addTag(name) {
    this.config.tags.push({
      name,
    });
  }

  async save() {
    await writeFileSync(
      __dirname + '/../../src/swagger/configs/' + this.version + '.json',
      JSON.stringify(this.config, null, 2),
    );

    return this.config;
  }
}

export default SwaggerGenerator;
