const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../dist/app');

const { expect } = chai;

chai.use(chaiHttp);

const testData = require('./test-data-full-stack.json');

describe('Contracts Test', async () => {
  if (!testData) {
    throw new Error('Test Data required!');
  }

  for (const data of testData) {
    if (!data.name) {
      return;
    }

    if (data.name === 'ContractCreatedEvent') {
      it('should create new contract if does not exists', (done) => {
        chai
          .request(app)
          .post('/api/v1/admin/contract/create')
          .send({
            contractId: data.contractId,
            premium: data.premium,
            startDate: data.startDate,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            expect(res.status).to.be.oneOf([200, 400]);
            expect(res).to.be.an('object');

            done();
          });
      });
    } else if (data.name === 'ContractTerminatedEvent') {
      it('should terminate an existing contract if exists', (done) => {
        chai
          .request(app)
          .put('/api/v1/admin/contract/terminate')
          .send({
            contractId: data.contractId,
            terminationDate: data.terminationDate,
          })
          .end((err, res) => {
            if (err) {
              return done(err);
            }

            expect(res.status).to.be.oneOf([200, 400]);
            expect(res).to.be.an('object');

            done();
          });
      });
    }
  }
});
